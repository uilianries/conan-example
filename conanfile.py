from conans import ConanFile, CMake


class ExampleConan(ConanFile):
    name = "example"
    version = "0.1.0"
    url = "http://gitlab.com/uilian/conan-example"
    author = "Uilian Ries <uilian@gmail.com>"
    settings = "os", "arch", "compiler", "build_type"
    generators = "cmake"
    exports = "LICENSE"
    exports_sources = "CMakeLists.txt", "src/*"
    description = "Example library package for conan.io"
    license = "MIT"
    requires = "zlib/1.2.11@conan/stable"

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package(self):
        self.copy("LICENSE", dst="licenses")
        cmake = CMake(self)
        cmake.configure()
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["example"]
