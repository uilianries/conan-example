#ifndef PROJECT_EXAMPLE_HPP
#define PROJECT_EXAMPLE_HPP

#include <string>

std::string compress(const std::string& data);

#endif //PROJECT_EXAMPLE_HPP
