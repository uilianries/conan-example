#include <iostream>
#include "example.hpp"

int main() {
    const std::string word("foobar");
    const std::string result = compress(word);
    std::cout << word << ": " << result << std::endl;
    return 0;
}